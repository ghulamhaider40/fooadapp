@extends('admin/master_layout')
 @section('data')


  <!-- Main Sidebar Container -->
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Add User</h1>
          </div><!-- /.col -->
          
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
      <section class="content">
      <div class="container-fluid">
      	@if(Session::has('error'))
                    <p class="alert alert-danger">{{ Session::get('error') }}</p>
                @endif
                @if(Session::has('success'))
                    <p class="alert alert-success">{{ Session::get('success') }}</p>
                @endif
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              
              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="{{route('post-user')}}">
              	@csrf
                <div class="card-body">
                	<div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="firstname">First Name</label>
                    <input type="text" name="first_name" class="form-control"  placeholder="First name">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="lastname">Last Name</label>
                    <input type="text" name="last_name" class="form-control" placeholder="Last name">
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="phone">Phone Number</label>
                    <input type="number" name="phone" class="form-control" placeholder="Phone number">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" placeholder="Email">
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password" placeholder="Password">
                  </div>
                  </div>
                  <div class="col-md-6">
                  <div class="form-group">
                    <label for="Confirmpassword">Confirm password</label>
                    <input type="password" class="form-control" placeholder="Confirm password">
                  </div>
              </div>

              <div class="col-md-6">
                  <div class="form-group">
                    <label for="Confirmpassword">User Role</label>
                    <select class="form-control" name="role">
                      <option value="">Select user role</option>
                      @if(isset($role))
                      @foreach($role as $rol)
                      <option value="{{$rol->id}}">
                        {{$rol->name}}
                      </option>
                      @endforeach
                      @endif
                    </select>
                  </div>
              </div>
                  
                <!-- /.card-body -->
                 <div class="col-md-6">
                  <div class="form-group">
                    <br>
                <div class="card-footer" style="background-color:unset !important; float: right;">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                </div>
                </div>
              </form>
            </div>
            <!-- /.card -->

            <!-- Form Element sizes -->
            
                  </div>
                  <div class="form-group">
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
</div>
@endsection
<!-- ./wrapper -->
@push('css')
 <link rel="stylesheet" href="{{ $admin_assets }}/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{ $admin_assets }}/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ $admin_assets }}/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ $admin_assets }}/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ $admin_assets }}/dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ $admin_assets }}/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ $admin_assets }}/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ $admin_assets }}/plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  @endpush
<!-- jQuery -->
@push('js')
<script src="{{ $admin_assets }}/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ $admin_assets }}/plugins/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ $admin_assets }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="{{ $admin_assets }}/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="{{ $admin_assets }}/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="{{ $admin_assets }}/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="{{ $admin_assets }}/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="{{ $admin_assets }}/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="{{ $admin_assets }}/plugins/moment/moment.min.js"></script>
<script src="{{ $admin_assets }}/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ $admin_assets }}/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="{{ $admin_assets }}/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<!-- <script src="{{ $admin_assets }}/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script> -->
<!-- AdminLTE App -->
<script src="{{ $admin_assets }}/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ $admin_assets }}/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ $admin_assets }}/dist/js/demo.js"></script>
@endpush

