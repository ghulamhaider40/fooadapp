<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
View::share('admin_assets' , asset('public/admin'));
Route::get('/', function () {
	echo exec('php artisan clear:cache');
    return view('welcome');
});


    Route::prefix('admin')->group(function () {
    Route::get('login','AdminController@index')->name('admin');
    Route::post('login','AdminController@login')->name('login');
    Route::group(['middleware' => ['UserAuth']], function () {
    Route::get('dashboard','AdminController@dashboard');
    Route::get('ad-user','AdminController@ad_user')->name('ad-user');
    Route::post('post-user','AdminController@post_user')->name('post-user');
    Route::get('view-all-user','AdminController@view_all_user')->name('view-all-user');
    Route::get('view-all-rider','AdminController@view_all_rider')->name('view-all-rider');
    Route::get('view-all-resturent','AdminController@view_all_resturent')->name('view-all-resturent');
    Route::get('ad-resturent','AdminController@ad_resturent')->name('ad-resturent');
    Route::get('logout','AdminController@logout')->name('logout');
    Route::get('delete-user/{id}','AdminController@delete_user')->name('delete-user');
    Route::get('delete-rider/{id}','AdminController@delete_rider')->name('delete-rider');
    Route::get('role','AdminController@role')->name('role');
    Route::get('permission','AdminController@permission')->name('permission');
    Route::get('edit-role/{id}','AdminController@edit-role')->name('edit-role');
    Route::get('delete-role/{id}','AdminController@delete_role')->name('delete-role');
    Route::post('post-permission','AdminController@post_permission')->name('post-permission');
    Route::post('post-role','AdminController@post_role')->name('post-role');
    Route::get('delete-permission/{id}','AdminController@delete_permission')->name('delete-permission');
    });
});