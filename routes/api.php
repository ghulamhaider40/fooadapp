<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('post-rider-des','ApiController@post_rider_des');
Route::get('get-rider-des','ApiController@get_rider_des');
Route::post('login','ApiController@login');
Route::get('logout','ApiController@logout');
Route::post('register','ApiController@register');
Route::post('rider-register','ApiController@rider_register');
Route::post('resturent-register','ApiController@resturent_register');
Route::post('post-order','ApiController@post_order');
Route::post('rider-document','ApiController@rider_document');
Route::post('resturent-login','ApiController@resturent_login');
Route::post('rider-login','ApiController@rider_login');
Route::post('post_resturent_dish','ApiController@post_resturent_dish');
Route::get('get-resturent/{lat}/{lng}','ApiController@get_resturent');
Route::post('login-with-social-media','ApiController@login_with_social_media');
Route::get('forget-password/{email}','ApiController@forget_password');
Route::post('forget','ApiController@forget');
Route::post('change-password','ApiController@change_password');
