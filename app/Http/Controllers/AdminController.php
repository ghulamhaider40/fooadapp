<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Hash;
use App\User;
use App\Rider;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;
use Session;
class AdminController extends Controller
{
	public function index()
	{

        if(Auth::guard('admins')->user())
        {
            return redirect('admin/dashboard');
        }
        else{

		return view('admin/login');
        }
	}
	public function dashboard()
	{
        
        if(\Auth::guard('admins')->User()->can('admin') OR \Auth::guard('admins')->User()->can('role manage'))
        {

        }
	     	return view('admin.index');
        // dd(\Auth::guard('admins')->User()->can('admin'));
        

	}
    public function post_role(Request $request)
    {
        if(\Auth::guard('admins')->User()->can('admin') OR \Auth::guard('admins')->User()->can('role manage'))
        {
        $role=new Role();
        $role->name=$request->name;
        $role->guard_name='admins';
        $role->save();
        if($request->permission)
        {
            foreach ($request->permission as $key => $value) {
                
             $role->givePermissionTo($value);
            }
        }
        Session::flash('success','Your role has been added successfully');
        return back();
    }
    else{
        return redirect('admin/login');
    }
    }
    public function delete_role($id)
    {
    if(\Auth::guard('admins')->User()->can('admin') OR \Auth::guard('admins')->User()->can('role manage'))
        {
        $id=decrypt($id);
        $role=Role::where('id',$id)->delete();
        Session::flash('success','Your role deleted successfully');
        return back();
    }
    else
    {
        return redirect('admin/login');
    }

    }
    public function delete_permission($id)
    {
        $id=decrypt($id);
        $permission=Permission::where('id',$id)->delete();
        Session::flash('success','Your permission has been deleted successfully');
        return back();
    }
    public function role()
    {
        $data['role']=Role::get();
        $data['permission']=Permission::get();
        return view('admin.role',$data);
    }
    public function post_permission(Request $request)
    {
        $permission=new Permission();
        $permission->name=$request->name;
        $permission->guard_name='admins';
        $permission->save();
        Session::flash('success','Your Permission has been added successfully');
        return back();
    }
    public function permission()
    {
        $data['permission']=Permission::get();
        return view('admin.permission',$data);
    }
    public function ad_user()
    {
        if(\Auth::guard('admins')->User()->can('admin') OR \Auth::guard('admins')->User()->can('user'))
        {

         $data['role']=Role::get();
         return view('admin/ad_user',$data);
        }
        else
        {
            Session::flash('error','You have no permission');
            return redirect('admin/login');
        }
    }
    public function delete_user($id)
    {
        if(\Auth::guard('admins')->User()->can('admin') OR \Auth::guard('admins')->User()->can('user'))
        {
        $id=decrypt($id);
        $user=User::where('id',$id)->delete();
        if($user)
        {
            Session::flash('success','user deleted successfully');
            return back();
        }
    }

    else{
        return redirect('admin/login');
    }
    }
    public function post_user(Request $request)
    {
        if(\Auth::guard('admins')->User()->can('admin') OR \Auth::guard('admins')->User()->can('user'))
        {
        $user=new User();
        $user->first_name=$request->first_name;
        $user->last_name=$request->last_name;
        $user->phone=$request->phone;
        $user->email=$request->email;
        $user->guard_name='admins';
        $user->password=Hash::make($request->password);
        $user->save();
        $user->assignRole($request->role);
        Session::flash('success','Your user added successfully');
        return redirect('admin/view-all-user');
    }
    else{
        return redirect('admin/login');
    }
    }
    public function view_all_user()
    {
        if(\Auth::guard('admins')->User()->can('admin') OR \Auth::guard('admins')->User()->can('user'))
        {

        $data['user']=User::orderBy('id','desc')->get();
        return view('admin/view_user',$data);
    }
    else{
        return redirect('admin/login');
    }
    }
    public function view_all_rider()
    {
    	$data['rider']=Rider::orderBy('id','desc')->get();
    	return view('admin.view_rider',$data);
    }
    public function delete_rider($id)
    {
    	$id=decrypt($id);
    	$rider=Rider::where('id',$id)->delete();
    	if($rider)
    	{
    		Session::flash('success','Rider deleted successfully');
    		return back();
    	}
    	else{
    		Session::flash('error','invalid input');
    		return back();
    	}
    }
    public function login(Request $request)
    {

    	if (Auth::guard('admins')->attempt(['email' => $request->email, 'password' => $request->password,'is_admin'=>1])) {

            return redirect('admin/dashboard');
        }
        else{
        	Session::flash('error','invalid email or password');
        	return redirect('admin/login');
        }
    }
    public function logout()
    {
        Auth::guard('admins')->logout();
        return redirect('admin/login');
    }
}
