<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\RiderRequest;
use App\Rider;
use App\User;
use App\OrderItem;
use App\Order;
use Hash;
use Auth;
use App\Resturent;
class ApiController extends Controller
{
	public function post_rider_des(Request $request)
	{
      $rider=Rider::find($request->id);
      if($rider)
      {
      $rider->lat=$request->lat;
      $rider->lag=$request->lag;
	  $rider->save();
	  $data['success']='true';
	  return json_encode($data);	
	  }
	  else{
	  	$data['success']='false';
	    return json_encode($data);
	  }
	}

	public function get_rider_des()
	{
		$rider=Rider::first();
		if($rider){
		$data['success']='true';
		$data['rider_des']=$rider;
		return json_encode($data);
		}
		else{
			$data['success']='false';
			$data['message']='Rider Not Found';
			return json_encode($data);
		}
	}

	public function login(Request $request)
	{
      
		if (Auth::attempt(['email' =>$request->email,'password'=>$request->password])) {
            $data['success']='true';
			$data['user']=Auth::User();
           return json_encode($data);     
        }
        else{
        	$data['success']='false';
        	$data['message']='User Not Found';
        	return json_encode($data);
        }
	}

	public function logout()
	{
		Auth::logout();
		return json_encode('logout');
	}

	public function register(Request $request)
	{
		$rules=[
        'first_name' => 'required',
        'last_name' => 'required',
        'email' =>  'bail|required|unique:users|max:255',
        'phone' => 'required',
        'password' => 'required',
         ];
         $Validator=\Validator::make($request->all(),$rules);
          if($Validator->fails())
          {
          	return response()->json($Validator->errors(),400);
          }
		$user=new User();
		$user->first_name=$request->first_name;
		$user->last_name=$request->last_name;
		$user->phone=$request->phone;
		$user->email=$request->email;
		$user->password=Hash::make($request->password);
		$user->save();
		if($user)
		{
			$data['success']='true';
			$data['user']=$user;

			return json_encode($data);
		}
		else{
			$data['success']='false';
			$data['message']='User register successfully';
			return json_encode($data);
		}
	}

	public function rider_register(Request $request){
		 $rules=[
        'phone' => 'required',
        'full_name' => 'required',
        'email' =>  'bail|required|unique:riders|max:255',
        'password' => 'required',
        'license_no' => 'required',
        'cnic_no' => 'required',
        'account_title' => 'required',
        'bank_account_no' => 'required',
        'bank_name' => 'required',
        'vehical_no' => 'required',
         ];
         $Validator=\Validator::make($request->all(),$rules);
          if($Validator->fails())
          {
          	return response()->json($Validator->errors(),400);
          }
		$rider=new Rider();
		$rider->full_name=$request->full_name;
		$rider->phone=$request->phone;
		$rider->email=$request->email;
		$rider->password=Hash::make($request->password);
		$rider->cnic_no=$request->cnic_no;
		$rider->license_no=$request->license_no;
		$rider->account_title=$request->account_title;
		$rider->bank_account_no=$request->bank_account_no;
		$rider->bank_name=$request->bank_name;
		$rider->vehical_no=$request->vehical_no;
		$rider->save();
		if($rider)
		{
			$data['success']='true';
			$data['rider']=$rider;
			return json_encode($data);
		}
		else{
			$data['success']='false';
			$data['message']='invalid input';
			return json_encode($data);
		}

	}

	public function resturent_register(Request $request)
	{
		 $rules=[
        'phone' => 'required',
        'name' => 'required',
        'email' =>  'bail|required|unique:resturents|max:255',
        'password' => 'required',
        'city' => 'required',
         ];
         $Validator=\Validator::make($request->all(),$rules);
          if($Validator->fails())
          {
          	return response()->json($Validator->errors(),400);
          }
        $resturent=new Resturent();
        $resturent->name=$request->name;
		$resturent->phone=$request->phone;
		$resturent->email=$request->email;
		$resturent->password=Hash::make($request->password);
		$resturent->city=$request->city;
		$resturent->save();
		if($resturent)
		{
			$data['success']='true';
			$data['resturent']=$resturent;
			return json_encode($data);
		}
		else{
			$data['success']='false';
			$data['message']='invalid';
			return json_encode($data);
		}

	}

	public function post_resturent_dish(Request $request)
	{
		$dish=new ResturentDish();
		$dish->dish_name=$request->dish_name;
		$dish->dish_price=$request->dish_price;
		$dish->description=$request->description;
		$dish->save();
		if($dish)
		{
			$data['success']='true';
			$data['resturent']=$dish;
			return json_encode($data);
		}
		else{
			$data['success']='false';
			$data['message']='invalid';
			return json_encode($data);
		}

	}

	public function post_order(Request $request)
	{
		$rules=[
        'resturent_id' => 'required',
        'location' =>  'required',
         ];
         $Validator=\Validator::make($request->all(),$rules);
          if($Validator->fails())
          {
          	return response()->json($Validator->errors(),400);
          } 
		$order=new Order();
		$order->rider_id=$request->rider_id;
        $order->resturent_id=$request->resturent_id;
        $order->location=$request->location;
        $order->save();
        $item=$request->item;
        if($item)
        {
        	foreach ($item as $key => $value) {
        		$itemorder=new OrderItem();
        		$itemorder->order_id=$order->id;
        		$itemorder->item_name=$request->item_name;
        		$itemorder->save();
        	}
        }

	if($order)
		{
			$data['success']='true';
			$data['resturent']=$order;
			return json_encode($data);
		}
		else{
			$data['success']='false';
			$data['message']='invalid';
			return json_encode($data);
		}
    }
}
