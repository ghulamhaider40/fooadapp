<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\RiderRequest;
use App\Rider;
use App\User;
use App\OrderItem;
use App\Order;
use Hash;
use Auth;
use App\Resturent;
use App\Mail\ForgetPassword;
use Illuminate\Support\Facades\Mail;
use DB;
class ApiController extends Controller
{
	public function post_rider_des(Request $request)
	{
      $rider=Rider::find($request->id);
      if($rider)
      {
      $rider->lat=$request->lat;
      $rider->lag=$request->lag;
	  $rider->save();
	  $data['success']='true';
	  return json_encode($data);
	  }
	  else{
	  	$data['success']='false';
	    return json_encode($data);
	  }
	}



	public function get_rider_des()
	{
		$rider=Rider::first();
		if($rider){
		$data['success']='true';
		$data['rider_des']=$rider;
		return json_encode($data);
		}
		else{
			$data['success']='false';
			$data['message']='Rider Not Found';
			return json_encode($data);
		}
	}

    public function login_with_social_media(Request $request)
    {
        $data['ErrorCode']=400;
        $data['success']='false';
        $user=User::where('email',$request)->first();
        if($user)
        {
            $data['ErrorCode']=200;
            $data['success']='true';
            $data['Message']='login successfully';
            return response()->json(array('userItem' => $user,'OperationStatus'=>$data));
        }
        else{
            $user=new User();
            $user->email=$request->email;
            $user->name=$request->name;
            $user->password=Hash::make('123456');
            $user->user_type='user';
            $user->save();
            $data['ErrorCode']=200;
            $data['success']='true';
            $data['Message']='login successfully';
            return response()->json(array('userItem' => $user,'OperationStatus'=>$data));

        }

    }
    public  function forget(Request $request)
    {
        $data['ErrorCode']=400;
        $data['success']='false';
        $user=User::where(['email'=>$request->email,'user_type'=>'user'])->first();
        if($user)
        {
            $data['ErrorCode']=200;
            $data['success']='true';
            $data['Message']='Send a link in your email to reset password';
            $userItem=$user;
            return response()->json(array('userItem'=>$userItem,'OperationStatus'=>$data));
            Mail::to($request->email)->send(new ForgetPassword($user));
        }
    else{
          $data['Message']='Email is invalid try again';
          $userItem=(object) array();
          return response()->json(array('userItem'=>$userItem,'OperationStatus'=>$data));
    }
    }
    public function change_password(Request $request)
    {
        $user= User::where(['email'=>$request->email,'user_type'=>'user'])->first();
        $user->password=Hash::make($request->password);
        $user->save();
        if($user)
        {
            $data['ErrorCode']=200;
            $data['success']='true';
            $data['Message']='Your pasword has been changed successfully';
            $userItem=$user;
            return response()->json(array('userItem' => $userItem,'OperationStatus'=>$data));
        }
        else{
            $data['ErrorCode']=400;
            $data['success']='false';
            $data['Message']='invalid email please try again';
            $userItem=(object)array();
            return response()->json(array('userItem' => $userItem,'OperationStatus'=>$data));
        }
    }
    public function forget_password($email)
    {
        $email=decrypt($email);
        $user= User::where(['email'=>$email,'user_type'=>'user'])->first();
        if (\Carbon\Carbon::now()->greaterThan($user->created_at->addDay())) {
            $data['ErrorCode']=400;
            $data['success']='false';
            $data['Message']='sorry your link has been expired';
            $userItem=(object)array();
            return response()->json(array('userItem' => $userItem,'OperationStatus'=>$data));
        }
              $data['ErrorCode']=200;
              $data['success']='false';
              $data['Message']='enter new password';
              $userItem=$user;
        return response()->json(array('userItem' => $userItem,'OperationStatus'=>$data));
    }
	 public function rider_login(Request $request)
	 {
	   $data['ErrorCode']=400;
        $data['success']='false';
       if(Auth::attempt(['email' => $request->email, 'password' => $request->password,'user_type'=>'rider'])) {
          $data['ErrorCode']=200;
          $data['success']='true';
          $data['Message']='login successfully';
          $rider=Auth::user();
          return response()->json(array('rider' => $rider,'OperationStatus'=>$data));
     	}
     	elseif(Auth::attempt(['phone' => $request->phone, 'password' => $request->password,'user_type'=>'rider']))
     	{
     	  $data['ErrorCode']=200;
          $data['success']='true';
          $data['Message']='login successfully';
          $rider=Auth::User();
          return response()->json(array('rider' => $rider,'OperationStatus'=>$data));
     	}
     		else
     		{
     	 $rider = (object) array();
     	 $data['Message']='invalid email or password';
     	  return response()->json(array('rider'=>$rider,'OperationStatus'=>$data));
     	}
	 }
     public function resturent_login(Request $request)
     {

        $data['ErrorCode']=400;
        $data['success']='false';
       if(Auth::attempt(['email' => $request->email, 'password' => $request->password,'user_type'=>'resturent'])) {
          $data['ErrorCode']=200;
          $data['success']='true';
          $data['Message']='login successfully';
          $resturent=Auth::User();
          return response()->json(array('resturent' => $resturent,'OperationStatus'=>$data));
     	}
     	elseif(Auth::attempt(['phone' => $request->phone, 'password' => $request->password,'user_type'=>'resturent']))
     	{
           $data['ErrorCode']=200;
          $data['success']='true';
          $data['Message']='login successfully';
          $resturent=Auth::User();
          return response()->json(array('resturent' => $resturent,'OperationStatus'=>$data));
     	}
     	else
     	{
     	 $resturent = (object) array();
     	 $data['Message']='invalid email or password';
     	  return response()->json(array('resturent'=>$resturent,'OperationStatus'=>$data));
     	}
     }
	public function login(Request $request)
	{
		$data['success']='false';
        $data['ErrorCode']='400';

		if(Auth::attempt(['email' =>$request->email,'password'=>$request->password,'user_type'=>'user'])) {
            $data['success']='true';
            $data['ErrorCode']='400';
            $data['Message']='login successfully';
			$userItem=Auth::User();
           return response()->json(array(
                		'userItem' =>$userItem,
                        'OperationStatus' => $data,
                         ));
        }
        elseif(Auth::attempt(['phone' =>$request->phone,'password'=>$request->password,'user_type'=>'user'])) {
            $data['success']='true';
            $data['ErrorCode']='400';
            $data['Message']='login successfully';
			$userItem=Auth::User();
           return response()->json(array(
                		'userItem' =>$userItem,
                        'OperationStatus' => $data,
                         ));
        }
        else{
        	$data['Message']='Invalid email or password';
                $userItem = (object) array();
              return response()->json(array(
                		'userItem' =>$userItem,
                        'OperationStatus' => $data,
                         ));
        }
	}

	public function logout()
	{
		Auth::logout();
		return json_encode('logout');
	}

	public function register(Request $request)
	{

            $data['success']='false';
          	$data['ErrorCode']='400';
          	$user=User::where(['email'=>$request->email,'user_type'=>'user'])->first();
          	if($user)
          	{
          		$data['Message']='Email already have a token';
                $userItem = (object) array();
              return response()->json(array(
                		'userItem' =>$userItem,
                        'OperationStatus' => $data,
                         ));
          	}
           $user=User::where(['phone'=>$request->phone,'user_type'=>'user'])->first();
          	if($user)
          	{
          		$data['Message']='Phone already have a token';
                $userItem = (object) array();
              return response()->json(array(
                		'riderItem' =>$userItem,
                        'OperationStatus' => $data,
                         ));

           $userItem = (object) array();
          return response()->json(array('userItem'=>$userItem,'OperationStatus'=>$data));
          }
		$user=new User();
		$user->name=$request->name;
		$user->user_type='user';
		$user->phone=$request->phone;
		$user->email=$request->email;
		$user->password=Hash::make($request->password);
		$user->save();
		if($user)
		{
			$data['success']='true';
			$data['ErrorCode']='200';
			$data['Message']='User register successfully';
			return response()->json(array('userItem' =>$user,'OperationStatus'=>$data));
		}
		else{
			$data['success']='false';
			$data['message']='User register successfully';
			return json_encode($data);
		}
	}

	public function rider_register(Request $request){

          	$data[ 'success']='false';
          	$data['ErrorCode']='400';
          	$user=User::where(['email'=>$request->email,'user_type'=>'rider'])->first();
          	if($user)
          	{
          		$data['Message']='Email already have a token';
                $riderItem = (object) array();
              return response()->json(array(
                		'riderItem' =>$riderItem,
                        'OperationStatus' => $data,
                         ));
          	}
           $user=User::where(['phone'=>$request->phone,'user_type'=>'rider'])->first();
          	if($user)
          	{
          		$data['Message']='Phone already have a token';
                $riderItem = (object) array();
              return response()->json(array(
                		'riderItem' =>$riderItem,
                        'OperationStatus' => $data,
                         ));

           $riderItem = (object) array();
          return response()->json(array('riderItem'=>$riderItem,'OperationStatus'=>$data));
          }
		$rider=new User();
		$rider->name=$request->name;
		$rider->phone=$request->phone;
		$rider->email=$request->email;
		$rider->user_type='rider';
		$rider->password=Hash::make($request->password);
		$rider->save();
		if($rider)
		{
			$data['success']='true';
			$data['ErrorCode']='200';
			$data['Message']='Rider register successfully';
			return response()->json(array('riderItem'=>$rider,'OperationStatus'=>$data));
		}
		else{

			$data['message']='invalid input';
			$riderItem = (object) array();
		   return response()->json(array('riderItem'=>$riderItem,'OperationStatus'=>$data));
		}

	}

	public function rider_document(Request $request)
	{
		$ErrorMessage='';
	    $data['success']='false';
        $data['ErrorCode']='400';
        if(Rider::find($request->id))
        {
          $rider=Rider::find($request->id);
        }
        else{

            $rider=new Rider();
        }
        $rider->id=$request->id;
        $rider->cnic_no=$request->cnic_no;
		$rider->license_no=$request->license_no;
		$rider->vehical_no=$request->vehical_no;
		$rider->save();
		if($rider)
		{
			$data['success']='true';
			$data['ErrorCode']='200';
			$data['Message']='Rider document register successfully';
			return response()->json(array('riderItem'=>$rider,'OperationStatus'=>$data));
		}
		else{
			$data['Message']='invalid input';
			$riderItem = (object) array();
			return response()->json(array('riderItem'=>$riderItem,'OperationStatus'=>$data));
		}


	}
	public function get_resturent($lat='',$lng='')
	{
		$resturent = User::select(DB::raw('*, (2*acos( cos( radians('.$lat.') ) * cos( radians(lat) ) * cos( radians(lng ) - radians('.$lng.') ) + sin( radians('.$lat.') ) * sin( radians( lat ) ) ) ) AS distance'))
            ->having('distance', '<', 2)
            ->orderBy('distance','asc')
            ->get();
		dd($resturent);
	}

	public function resturent_register(Request $request)
	{
		    $ErrorMessage='';
          	$data[ 'success']='false';
          	$data['ErrorCode']='400';
          	$user=User::where(['email'=>$request->email,'user_type'=>'resturent'])->first();
          	if($user)
          	{
          		$data['Message']='Email already have a token';
                $resturentItem = (object) array();
              return response()->json(array(
                		'resturentItem' =>$resturentItem,
                        'OperationStatus' => $data,
                         ));
          	}
           $user=User::where(['phone'=>$request->phone,'user_type'=>'resturent'])->first();
          	if($user)
          	{
          		$data['Message']='Phone already have a token';
                $resturentItem = (object) array();
              return response()->json(array(
                		'resturentItem' =>$resturentItem,
                        'OperationStatus' => $data,
                         ));
          	}
        $resturent=new User();
        $resturent->name=$request->name;
		$resturent->phone=$request->phone;
		$resturent->email=$request->email;
		$resturent->password=Hash::make($request->password);
		// $resturent->city=$request->city;
		$resturent->user_type='resturent';
		$resturent->save();
		if($resturent)
		{
			$data['ErrorCode']='200';
			$data['success']='true';
			$data['Message']='Resturent register successfully';
			$data1['resturentItem']=$resturent;
			return response()->json(array(
                        'OperationStatus' => $data,
                         'resturentItem' => $resturent,

                            ));
		}
		else{
			$data['message']='invalid input';
			$resturentItem = (object) array();
			return response()->json(array(
                        'OperationStatus' => $data,
                         'resturentItem' =>$resturentItem,

                            ));
		}

	}


	public function post_resturent_dish(Request $request)
	{
		$dish=new ResturentDish();
		$dish->resturent_id=$request->resturent_id;
		$dish->dish_name=$request->dish_name;
		$dish->dish_price=$request->dish_price;
		$dish->description=$request->description;
		$image = $request->dish_image;
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10).'.'.'png';
		$dish->save();
		if($dish)
		{
			$data['success']='true';
			$data['resturent']=$dish;
			return json_encode($data);
		}
		else{
			$data['success']='false';
			$data['message']='invalid';
			return json_encode($data);
		}

	}

	public function post_order(Request $request)
	{
		$rules=[
        'resturent_id' => 'required',
        'location' =>  'required',
         ];
         $Validator=\Validator::make($request->all(),$rules);
          if($Validator->fails())
          {
          	return response()->json($Validator->errors(),400);
          }
		$order=new Order();
		$order->rider_id=$request->rider_id;
        $order->resturent_id=$request->resturent_id;
        $order->location=$request->location;
        $order->save();
        $item=$request->item;
        if($item)
        {
        	foreach ($item as $key => $value) {
        		$itemorder=new OrderItem();
        		$itemorder->order_id=$order->id;
        		$itemorder->item_name=$request->item_name;
        		$itemorder->save();
        	}
        }

	if($order)
		{
			$data['success']='true';
			$data['resturent']=$order;
			return json_encode($data);
		}
		else{
			$data['success']='false';
			$data['message']='invalid';
			return json_encode($data);
		}
    }
}
